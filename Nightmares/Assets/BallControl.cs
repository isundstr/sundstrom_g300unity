﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour {

    Rigidbody rb;

    Vector3 input;
    public float speed;

	// Use this for initialization
	void Start () {
        Physics.gravity = Physics.gravity * 2f;
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

	}

    private void FixedUpdate()
    {
        rb.velocity += input * speed * Time.deltaTime;
    }



}
